﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.PlatformAbstractions;
using System.IO;
using System.Collections.Generic;
using Steeltoe.Discovery.Client;
using NLog.Extensions.Logging;
using NLog.Config;
using Agora.Brokerage.Dil.HelloWorldService.Models;
using Agora.Brokerage.Dil.HelloWorldService.Services;
using Steeltoe.CircuitBreaker.Hystrix;
using Steeltoe.Management.Endpoint.Health;
using Steeltoe.Management.Endpoint.Trace;
using Steeltoe.Management.Endpoint.Info;
using Steeltoe.Management.Endpoint.Loggers;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using Agora.Brokerage.Dil.HelloWorldService.Configuration;


#pragma warning disable 1591

namespace Agora.Brokerage.Dil.HelloWorldService
{
    //
    // NOTE: This sample application assumes a running Spring Cloud Config Server is started
    //       with repository data for application named: foo, & profile: development
    //
    //       The easiest way to get that to happen is clone the spring-cloud-config
    //       repo and run the config-server.
    //          eg. git clone https://github.com/spring-cloud/spring-cloud-config.git
    //              cd spring-cloud-config\spring-cloud-config-server
    //              mvn spring-boot:run
    //
    public class Startup
    {
        private readonly IConfiguration configuration;
        public Startup(IConfiguration configuration) => this.configuration = configuration;
        public IConfiguration Configuration => configuration;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                        .SetBasePath(env.ContentRootPath)
                        .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);

            builder.AddEnvironmentVariables();
            configuration = builder.Build();

        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.AddMvc();
            AddDataServices(services);
            AddSteeltoeServices(services);
            AddConfigurationOptions(services);
            AddInternalServices(services);
            AddActuatorServices(services);
            AddSwaggerClient(services);
        }

        private void AddSteeltoeServices(IServiceCollection services)
        {
            services.AddDiscoveryClient(Configuration);

            // Setup Hystrix metrics
            services.AddHystrixMetricsStream(Configuration);

        }

        private void AddConfigurationOptions(IServiceCollection services)
        {
            //Mapping to configuartion section in appsettings
            // Add the configuration data returned from the Spring Cloud Config Server as IOption<>
            services.Configure<ConfigServerData>(Configuration);
            services.Configure<HelloWorldOptions>(configuration);
        }

        private void AddActuatorServices(IServiceCollection services)
        {
            services.AddHealthActuator(configuration);
            services.AddTraceActuator(configuration);
            services.AddLoggersActuator(configuration);
            services.AddInfoActuator(configuration);
        }

        private void AddInternalServices(IServiceCollection services)
        {
            services.AddSingleton<IConfigService, ConfigService>();
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddTransient<ITodoService, TodoService>();
            services.AddHystrixCommand<ConfigServiceHealthCommand>("ConfigServiceHealth", Configuration);
        }

        private static void AddSwaggerClient(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info
                {
                    Title = "Sample Web Api",
                    Version = "v1"
                });
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "dil-hello-world-svc.xml");
                c.IncludeXmlComments(xmlPath);
            });
        }

        private void AddDataServices(IServiceCollection services)
        {
            services.AddDbContext<TodoContext>(opt => opt.UseInMemoryDatabase("TodoList"));

            // leverage the same properties as Spring does for Redis connection to avoid duplicate settings in config server

            if ( this.isRedisDefined() )
            {
                services.AddDistributedRedisCache(option =>
                {
                    string redisHost = this.configuration.GetValue<string>("spring:redis:host");
                    string redisPort = this.configuration.GetValue<string>("spring:redis:port");

                    option.Configuration = string.Concat(redisHost, !string.IsNullOrEmpty(redisPort) ? ":"+redisPort : string.Empty);
                    option.InstanceName = this.configuration.GetValue<string>("spring:redis:instanceName");
                });
            }
        }

        private bool isRedisDefined(){
            return !string.IsNullOrEmpty( this.configuration.GetValue<string>("spring:redis:host"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        // The swagger endpoint is designed to work with eureka and zuul routes
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            } 
            ConfigureLogging(loggerFactory, env);
            AddCommonAppServices(app);
            AddSwaggerSupport(app);
            AddSteeltoe(app);
            AddActuators(app);
        }

        private static void AddSwaggerSupport(IApplicationBuilder app)
        {
            app.UseSwagger().UseSwaggerUI(c => c.SwaggerEndpoint("/api/hello-world/swagger/v1/swagger.json", "Sample Web Api V1"));
        }

        private static void AddCommonAppServices(IApplicationBuilder app)
        {
            app.UseMvc();
        }

        private static void AddSteeltoe(IApplicationBuilder app)
        {
            app.UseDiscoveryClient();
            app.UseHystrixRequestContext().UseHystrixMetricsStream();
        }

        private static void ConfigureLogging(ILoggerFactory loggerFactory, IHostingEnvironment env)
        {
            loggerFactory.ConfigureNLog($"nlog.{env.EnvironmentName}.config");
        }

        private static void AddActuators(IApplicationBuilder app)
        {
            app.UseHealthActuator();
            app.UseTraceActuator();
            app.UseInfoActuator();
            app.UseLoggersActuator();
        }

    }
}
