using System.Collections.Generic;
using System.Threading.Tasks;
using Agora.Brokerage.Dil.HelloWorldService.Models;

#pragma warning disable 1591

namespace Agora.Brokerage.Dil.HelloWorldService.Services
{
    public interface ITodoService
    {
        IEnumerable<TodoItem> Get();
        TodoItem Find(long id);
        TodoItem Add(TodoItem todoItem);
        TodoItem Update(TodoItem todoItem);
        bool Delete(long id);
    }
}