using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Steeltoe.CircuitBreaker.Hystrix;

#pragma warning disable 1591

namespace Agora.Brokerage.Dil.HelloWorldService.Services
{
    public class ServiceCommandGenerator<U, T> : HystrixCommand<T>
    {
        private U argument;
        private Func<U, T> action;
        private Func<U, T> fallback;
        public ServiceCommandGenerator(string serviceName, U argument, Func<U, T> action, Func<U, T> fallback, ILogger logger) 
            : base(HystrixCommandGroupKeyDefault.AsKey(serviceName + "Group"), null, null, logger)
        {
            this.argument = argument;
            this.action = action;
            this.fallback = fallback;
        }
        public async Task<T> Exec() => await ExecuteAsync();
        public async Task<T> Exec(CancellationToken token) => await ExecuteAsync(token);
        protected override async Task<T> RunAsync() => await Task.Run(() => { return action(argument); });
        protected override async Task<T> RunFallbackAsync() => await Task.Run(() => { return fallback(argument); });
    }
    public class ServiceCommandGenerator<T> : HystrixCommand<T>
    {
        public ServiceCommandGenerator(string serviceName, Func<T> action, Func<T> fallback, ILogger logger) 
            : base(HystrixCommandGroupKeyDefault.AsKey(serviceName + "Group"), action, fallback, logger)
        {
        }
        public async Task<T> Exec() => await ExecuteAsync();
        public async Task<T> Exec(CancellationToken token) => await ExecuteAsync(token);
    }

    public static class HystrixRunner
    {
        public static async Task<T> ExecuteAsync<T, SType>(this SType s, Func<T> action, Func<T> fallback = null, ILogger logger = null) => 
            await new ServiceCommandGenerator<T>(typeof(SType).Name, action, fallback, logger).ExecuteAsync();
        public static async Task<T> ExecuteAsync<T, SType>(this SType s, IHystrixCommandOptions options, Func<T> action, CancellationToken token, Func<T> fallback = null, ILogger logger = null) =>
            await new ServiceCommandGenerator<T>(typeof(SType).Name, action, fallback, logger).ExecuteAsync(token);
        public static async Task<T> ExecuteAsync<U, T, SType>(this SType s, U argument, Func<U, T> action, Func<U, T> fallback = null, ILogger logger = null) => 
            await new ServiceCommandGenerator<U, T>(typeof(SType).Name, argument, action, fallback, logger).ExecuteAsync();
        public static async Task<T> ExecuteAsync<U, T, SType>(this SType s, U argument, Func<U, T> action, CancellationToken token, Func<U, T> fallback = null, ILogger logger = null) => 
            await new ServiceCommandGenerator<U, T>(typeof(SType).Name, argument, action, fallback, logger).ExecuteAsync();
    }
}