using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Agora.Brokerage.Dil.HelloWorldService.Models;
using Microsoft.Extensions.Logging;

#pragma warning disable 1591

namespace Agora.Brokerage.Dil.HelloWorldService.Services
{
    public class TodoService : ITodoService
    {
        private readonly TodoContext todoContext;
        private readonly ILogger logger;
        public TodoService(TodoContext todoContext, ILogger<TodoService> logger)
        {
            this.todoContext = todoContext;
            this.logger = logger;
            Initialize();
        }

        public TodoItem Add(TodoItem todoItem)
        {
            if (todoItem == null)
                return null;
            todoContext.TodoList.Add(todoItem);
            todoContext.SaveChanges();
            return todoItem;;
        }

        public bool Delete(long id)
        {
            var todo = todoContext.TodoList.FirstOrDefault(t => t.Id == id);
            if (todo == null)
                return true;
            try
            {
                todoContext.TodoList.Remove(todo);
                todoContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public IEnumerable<TodoItem> Get() => todoContext.TodoList.AsEnumerable();

        public TodoItem Find(long id) => todoContext.TodoList.FirstOrDefault(t => t.Id == id);

        public TodoItem Update(TodoItem todoItem)
        {
            if (todoItem == null || todoItem.Id <= 0)
                return null;
            var todo = Find(todoItem.Id);
            if (todo == null)
                return null;
            if(!string.IsNullOrWhiteSpace(todoItem.Name))
                todo.Name = todoItem.Name;
            if (!string.IsNullOrWhiteSpace(todoItem.Description))
                todo.Description = todoItem.Description;
            if (todo.IsComplete != todoItem.IsComplete)
                todo.IsComplete = todoItem.IsComplete;
            todoContext.TodoList.Update(todo);
            todoContext.SaveChanges();
            return todo;
        }

        private void Initialize()
        {
            if (this.todoContext.TodoList.Count() == 0)
            {
                this.todoContext.TodoList.Add(
                    new TodoItem { Name = "Default", Description = "Default Item", IsComplete = false }
                    );
                this.todoContext.SaveChanges();
            }
        }
    }
}