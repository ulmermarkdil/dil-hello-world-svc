using Agora.Brokerage.Dil.HelloWorldService.Models;
using Microsoft.EntityFrameworkCore;

#pragma warning disable 1591

namespace Agora.Brokerage.Dil.HelloWorldService.Services
{
    public class TodoContext : DbContext
    {
        public TodoContext(DbContextOptions<TodoContext> options)
            : base(options)
        {
        }

        public DbSet<TodoItem> TodoList { get; set; }

    }
}