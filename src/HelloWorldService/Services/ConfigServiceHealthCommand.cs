using Microsoft.Extensions.Logging;
using Steeltoe.CircuitBreaker.Hystrix;
using System.Threading.Tasks;
using Steeltoe.Common.Discovery;
using System.Net.Http;
using System;

#pragma warning disable 1591

namespace Agora.Brokerage.Dil.HelloWorldService.Services
{
    public class ConfigServiceHealthCommand : HystrixCommand<string>
    {
        private readonly ILogger logger;
        private readonly IConfigService configService;
        public ConfigServiceHealthCommand(
            IHystrixCommandOptions options, 
            IConfigService configService, 
            ILogger<ConfigServiceHealthCommand> logger) 
            : base(options)
        {
            this.logger = logger;
            this.configService = configService;
            IsFallbackUserDefined = true;
        }

        public async Task<string> GetHealth()
        {
            logger.LogInformation("GetHealth !!");
            return await ExecuteAsync();
        }
        protected override async Task<string> RunAsync()
        {
            logger.LogInformation("RunAsync !!");
            var result = await configService.Health();
            logger.LogInformation("Run: {0}", result);
            return result;
        }

        protected override async Task<string> RunFallbackAsync()
        {
            logger.LogInformation("RunFallbackAsync !!");
            logger.LogInformation("Running config service health fall back");
            return await Task.Run(() => { return "Config service not reached."; });
        }
    }
}