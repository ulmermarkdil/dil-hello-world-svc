using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Steeltoe.Common.Discovery;

#pragma warning disable 1591

namespace Agora.Brokerage.Dil.HelloWorldService.Services
{
    public class ConfigService : IConfigService
    {
        private readonly ILogger logger;
        private readonly DiscoveryHttpClientHandler handler;
        public ConfigService(ILogger<ConfigService> logger, IDiscoveryClient client)
        {
            this.logger = logger;
            this.handler = new DiscoveryHttpClientHandler(client);
        }

        public async Task<string> Health()
        {
            logger.LogInformation("Health !!");
                logger.LogInformation("Getting config server health");
                using (var client = new HttpClient(handler, false))
                {
                    return await client.GetStringAsync("http://dil-config-svc/manage/health");
                }
        }
    }
}