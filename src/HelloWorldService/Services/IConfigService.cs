using System.Threading.Tasks;

#pragma warning disable 1591

namespace Agora.Brokerage.Dil.HelloWorldService.Services
{
    public interface IConfigService
    {
        Task<string> Health();
    }
}