﻿using System;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using NLog;
using NLog.Web;
using NLog.LayoutRenderers;
using Steeltoe.Extensions.Configuration.ConfigServer;



#pragma warning disable 1591

namespace Agora.Brokerage.Dil.HelloWorldService
{
    public class Program
    {

        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }
        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseUrls(GetServerUrls( args))
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var env = hostingContext.HostingEnvironment;
                    config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);
                    config.AddEnvironmentVariables();
                })
                .AddConfigServer() // connect to the Spring Cloud Config server via Steeltoe, failfast=true will cause the application to halt here until connected
                .UseStartup<Startup>()
                .ConfigureLogging((context, logging) => // Remove any logging setup by the CreateDefaultBuilder() call, we will add back in what we need
                {
                    logging.ClearProviders();
                })
                .UseNLog() // Add NLog provider back into the logging mix
                .Build();

        private static string[] GetServerUrls(string[] args) => args.Length < 2
                ? new string[0]
                : args.Zip(args.Skip(1), (a, b) =>
                    "--server.urls".Equals(a, StringComparison.OrdinalIgnoreCase) ? b : String.Empty)
                  .Where(item => !string.IsNullOrWhiteSpace(item))
                  .Select(item => item.Trim())
                  .ToArray();
    }
    
}
