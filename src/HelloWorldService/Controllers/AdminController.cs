using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Microsoft.Extensions.Configuration;


using Agora.Brokerage.Dil.HelloWorldService.Models;
using System.Linq;
using Agora.Brokerage.Dil.HelloWorldService.Configuration;
using Microsoft.Extensions.Options;

namespace Agora.Brokerage.Dil.HelloWorldService.Controllers
{
    /// <summary>
    /// Administrative endpoints to provide insight into the internals of the microservice
    /// </summary>
    public class AdminController : Controller
    {
        private readonly ILogger logger;
        private readonly IConfiguration configuration;

        private readonly HelloWorldOptions _options;
        
        /// <summary>
        /// ctor
        /// </summary>
        public AdminController(IConfiguration configuration, ILogger<AdminController> logger,      
                                IOptions<HelloWorldOptions> options
                                )
        {
            this.logger = logger;
            this.configuration = configuration;          
            this._options=options.Value;
        }
        /// <summary>
        /// Get all configuration properties items.
        /// </summary>
        /// <returns>List of key value pairs in the configuration.</returns>
        [HttpGet]
        [Route("admin/env")]
        public IEnumerable<KeyValuePair<string, string>> GetEnv(){
            logger.LogInformation($"{logger.GetType()}-GetEnv()");
            return this.configuration.AsEnumerable();
        } 

         /// <summary>
        /// POC: Get Logging configuration properties from appsettings.json.
        /// </summary>

        [HttpGet]
         [Route("admin/loggingVariables")]
        public IEnumerable<string> GetLoggingConfiguration()
        {
            logger.LogInformation($"{logger.GetType()}-GetLoggingConfiguration()");
            var defaultLogging = _options.logging.debug.logLevel.Default;
            return new string[] { "loggingConfigs.debug.logLevel.Default", defaultLogging };
        }

         /// <summary>
        /// POC: Get default string appsettings.json.
        /// </summary>
         [HttpGet]
         [Route("admin/defaultString")]
        public IEnumerable<string> GetStringConfiguration()
        {
            logger.LogInformation($"{logger.GetType()}-GetStringConfiguration()");
             return new string[] { "DefaultString is ",_options.testingString.Default };
        }

         /// <summary>
        /// POC: Get sftp details appsettings.json.
        /// </summary>
         [HttpGet]
         [Route("admin/sftp")]
        public IEnumerable<string> GetsftpConfiguration()
        {
            logger.LogInformation($"{logger.GetType()}-GetsftpConfiguration()");
             return this._options.sftp.Select(a=>a.Id);
        }
    }
}