using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Caching.Redis;
using Microsoft.Extensions.Caching.Distributed;
using System.Diagnostics;


using Agora.Brokerage.Dil.HelloWorldService.Models;
using System.Linq;
using Microsoft.Extensions.Logging.Console;

namespace Agora.Brokerage.Dil.HelloWorldService.Controllers
{
    /// <summary>
    /// Endpoints to provide caching
    /// </summary>
    [Route("cache")]
    [Produces("application/json")]
    public class CacheController : Controller
    {
        private readonly ILogger logger;
        private readonly IConfiguration configuration;
                private readonly IDistributedCache cache;

        /// <summary>
        /// ctor
        /// </summary>
        public CacheController(IConfiguration configuration, ILogger<AdminController> logger, IDistributedCache cache)
        {
            this.logger = logger;
            this.configuration = configuration;
            this.cache = cache;

            logger.LogDebug("CTOR: configuration:[{0}] cache:[{1}]", configuration, cache);
    
        }

        /**
         * GET key from cache
         */
        [HttpGet( "{key}")]
        public async Task<KeyValuePair<string,string>> Cache(string key){
            logger?.LogDebug("Cache GET called [{0}]", key);
            string value = await  this.cache.GetStringAsync( key);

            KeyValuePair<string,string> kvp = KeyValuePair.Create(key, value);
            return kvp;
        } 

        /**
        POST a key and value to the cache
         */
        [HttpPost( "{key}/{value}")]
        public async Task<KeyValuePair<string,string>> Cache(string key, string value){
            logger?.LogDebug("Cache POST called key:[{0}] value:[{1}]", key, value);
            await this.cache.SetStringAsync(key, value);
            KeyValuePair<string,string> kvp = KeyValuePair.Create(key, value);
            return kvp;

        } 




    }
}