using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Agora.Brokerage.Dil.HelloWorldService.Models;
using System.Linq;
using Agora.Brokerage.Dil.HelloWorldService.Services;
using System.Threading.Tasks;
using Steeltoe.CircuitBreaker.Hystrix;
using System.Threading;

namespace Agora.Brokerage.Dil.HelloWorldService.Controllers
{
    /// <summary>
    /// Simple Api to illustrate a microservice in dotNet Core
    /// </summary>
    [Route("Todo")]
    public class TodoController : Controller
    {
        private readonly ILogger logger;
        private readonly ITodoService service;

        /// <summary>
        /// ctor
        /// </summary>
        public TodoController(ITodoService service, ILogger<TodoController> logger)
        {
            this.service = service;
            this.logger = logger;
        }
        /// <summary>
        /// Get all todo items.
        /// </summary>
        /// <returns>List of todo items.</returns>
        [HttpGet]
        public async Task<IEnumerable<TodoItem>> GetAll() => await service.ExecuteAsync(service.Get, () => new List<TodoItem>(), logger);

        /// <summary>
        /// Get a single todo item by its id
        /// </summary>
        /// <param name="id">Id of the item to fetch.</param>
        /// <returns>Item, if found.</returns>
        [HttpGet("{id}", Name = "GetTodo")]
        public async Task<IActionResult> GetById(long id)
        {
            var todo = await service.ExecuteAsync(id, service.Find, _ => null, logger);
            return todo == null ? (IActionResult)NotFound() : new OkObjectResult(todo);
        }

        /// <summary>
        /// Add a todo item to cached list.
        /// </summary>
        /// <param name="item">Item in JSON format.</param>
        /// <returns>Url to item in header.</returns>
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] TodoItem item)
        {
            var todo = await service.ExecuteAsync(item, service.Add, _ => null, logger);
            return todo == null ? (IActionResult)BadRequest() : CreatedAtRoute("GetTodo", new { id = todo.Id }, todo);
        }

        /// <summary>
        /// Update a todo item.
        /// </summary>
        /// <param name="item">Item in JSON format.</param>
        /// <returns>None. NoContent Http response.</returns>
        [HttpPut]
        public async Task<IActionResult> Update([FromBody] TodoItem item)
        { 
            var todo = await service.ExecuteAsync(item, service.Update, _ => null, logger);
            return todo == null ? (IActionResult) BadRequest() : NoContent();
        }

        /// <summary>
        /// Delete a cached todo item.
        /// </summary>
        /// <param name="id">Id of item to be deleted.</param>
        /// <returns>None. NoContent Http response.</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            var result = await service.ExecuteAsync(id, service.Delete, _ => false, logger);
            return result ? (IActionResult)NoContent() : NotFound();
        }
    }
}