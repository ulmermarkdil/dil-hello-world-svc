using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Agora.Brokerage.Dil.HelloWorldService.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Steeltoe.Common.Discovery;

namespace Agora.Brokerage.Dil.HelloWorldService.Controllers
{
    /// <summary>
    /// Simple Api to illustrate a microservice in dotNet Core
    /// </summary>
    [Route("configsvchealth")]
    public class ConfigSvcHealth : Controller
    {
        private readonly ILogger logger;
        private readonly ConfigServiceHealthCommand healthCommand;

        /// <summary>
        /// ctor
        /// </summary>
        public ConfigSvcHealth(ILogger<ConfigSvcHealth> logger, ConfigServiceHealthCommand healthCommand)
        {
            this.logger = logger;
            this.healthCommand = healthCommand;
        }
        /// <summary>
        /// Pass through to config service.
        /// </summary>
        /// <returns>Whatever we get.</returns>
        [HttpGet]
        public async Task<string> Get()
        {
            logger.LogInformation("Get !!");
            return await healthCommand.GetHealth();
        }
    }
}