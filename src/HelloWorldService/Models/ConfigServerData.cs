﻿
#pragma warning disable 1591

namespace Agora.Brokerage.Dil.HelloWorldService.Models
{
    /// <summary>
    /// An object used with the DI Options mechanism for exposing the data retrieved 
    /// from the Spring Cloud Config Server
    /// </summary>
    public class ConfigServerData
    {
        public string Bar { get; set; }
        public string Foo { get; set; }
        public Info Info { get; set; }

    }

    public class Info
    {
        public string Description { get; set; }
        public string Url { get; set; }
    }
}
