namespace Agora.Brokerage.Dil.HelloWorldService.Models
{
    /// <summary>
    /// Simple todo item.
    /// </summary>
    public class TodoItem
    {
        /// <summary>
        /// Id of the item. Assigned when saved.
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// Reference name of item.
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Descriptive comment.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Flag indicating whether todo item has been completed.
        /// </summary>
        public bool IsComplete { get; set; }
    }
}