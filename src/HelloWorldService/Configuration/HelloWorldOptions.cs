#pragma warning disable 1591
namespace Agora.Brokerage.Dil.HelloWorldService.Configuration
{
 public class HelloWorldOptions
 {
        public LoggingConfig logging  {get; set;}
        public Sftp[]  sftp {get; set;}
        public TestingString testingString  {get; set;}

 }
}