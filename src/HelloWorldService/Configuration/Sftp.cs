using System.Collections.Generic;

#pragma warning disable 1591

namespace Agora.Brokerage.Dil.HelloWorldService.Configuration
{
  public class Sftp
    {
      public string Id { get; set; }
      public SftpSettings Settings   { get; set; }
    }

}