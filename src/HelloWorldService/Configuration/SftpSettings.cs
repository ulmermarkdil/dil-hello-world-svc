#pragma warning disable 1591

namespace Agora.Brokerage.Dil.HelloWorldService.Configuration
{
  public class SftpSettings
    {
        public string Host { get; set; }
        public string Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}