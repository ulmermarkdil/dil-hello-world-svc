#pragma warning disable 1591
namespace Agora.Brokerage.Dil.HelloWorldService.Configuration
{

  public class LoggingConfig
    {
        public string IncludeScopes { get; set; }
        public Debug debug { get; set; }
    }
}