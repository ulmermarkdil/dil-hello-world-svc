using NUnit.Framework;
using Agora.Brokerage.Dil.HelloWorldService;
using Agora.Brokerage.Dil.HelloWorldService.Common;

namespace Agora.Brokerage.Dil.HelloWorldService.Common
{
    public class TestCommon
    {
        private Common common;

        [SetUp]
        public void Setup(){
            common= new Common();
        }
        [Test]
        public void TestGreetingMessage()
        {
            string expected="Welcome Sushma,";
            string actual = common.getGreeetingMessage("Sushma");
            StringAssert.Contains(expected,actual);
        }
    }
}